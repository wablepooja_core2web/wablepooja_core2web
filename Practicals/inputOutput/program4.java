// Write a program to reverse the number 
//
import java.io.*;
class program4 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a number :");
                int num=Integer.parseInt(br.readLine());
		
		int temp=num;
                int digit=0;
		int rev=0;

                while(num!=0) {
                      	digit=num%10;
			rev=rev*10 + digit;
                        
                        num/=10;
                }
		System.out.println("Reverse of "+temp+" is "+rev);
        }
}
