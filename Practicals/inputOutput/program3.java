// Write a program to print the factorial of a number
//
import java.io.*;
class program3 {
        public static void main(String args[]) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter a number :");
                int num=Integer.parseInt(br.readLine());

                int temp=num;
		int fact=1;

                while(num>=1) {
                        fact=num*fact;

                        num--;
                }
		System.out.println("Factorial of "+temp+" is "+fact);
        }
}
