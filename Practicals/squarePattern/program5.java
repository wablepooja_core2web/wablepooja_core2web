/* row = 3
 *
 * 3  16  5
 * 36 7   64
 * 9  100 11
 *
 * Row = 4
 * 16  5  36  7
 * 64  9  100 11
 * 144 13 196 15
 * 256 17 324 19   */

import java.io.*;
class program5 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(num%2==0) {
                                        System.out.print(num*num +" ");
                                }
                                else {
                                        System.out.print(num +" ");
                                }
                                num++;
                        }
                        System.out.println();
                }
        }
}
