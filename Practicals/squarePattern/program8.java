/* row = 3
 *
 * # C #
 * C # B
 * # C #
 *
 * Row = 4
 * # C # D
 * D # C #
 * # D # C
 * D # C #		*/

import java.io.*;
class program8 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
                
                for(int i=1;i<=row;i++) {
			int num = row+64;
                        for(int j=1;j<=row;j++) {
				if(i%2==1) {
                                	if(j%2==1) {
                                        	System.out.print("# ");
                               		 }
                                	else {
                                        	System.out.print((char)num +" ");
						num--;
                               		 }
				}
				else {
					if(j%2==0) {
                                                System.out.print("# ");
                                         }
                                        else {
                                                System.out.print((char)num +" ");
						num--;
                                         }
				}
                        }
                        
                        System.out.println();
                }
        }
}
