/* row = 3
 *
 * 9  4  5
 * 36 7  8
 * 81 10 11
 *
 * Row = 4
 * 16  5  6  7
 * 64  9  10 11
 * 144 13 14 15
 * 256 17 18 19     */

import java.io.*;
class program3 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());

                int num=row;
                for(int i=1;i<=row;i++) {

                        for(int j=1;j<=row;j++) {
                                if(j==1) {
                                        System.out.print(num*num +" ");
                                }
                                else {
                                        System.out.print(num +" ");
                                }
                                num++;
                        }
                        System.out.println();
                }
        }
}
