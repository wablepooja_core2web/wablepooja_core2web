// write a program to print the square of odd digit
// from the given number(first reverse the number the perform operation) 
//
import java.io.*;
class program10 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no :");
                int num = Integer.parseInt(br.readLine());

		int n=num;

		int rev=0;
		int digit=0;

		while(num!=0) {

			digit=num%10;

			if(digit%2==1) {
				System.out.print(digit*digit +",");
			}
			rev=rev*10+digit;

			num/=10;
		}
		System.out.println("Reverse no is : "+rev);
	}
}

