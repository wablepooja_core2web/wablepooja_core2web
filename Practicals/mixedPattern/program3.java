/* row = 3
 *
 * C B A
 * 1 2 3
 * C B A
 *
 * row = 4
 *
 * D C B A
 * 1 2 3 4
 * D C B A
 * 1 2 3 4	*/

import java.io.*;
class program3 {
        public static void main(String args[]) throws IOException {
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of row :");
                int row = Integer.parseInt(br.readLine());

             
                for(int i=1;i<=row;i++){
			int ch=64+row;
                        for(int j=1;j<=row;j++){
				
				
				if(i%2==1) {
					System.out.print((char)ch-- +" ");
					
				}
				else {
                                	System.out.print(j +" ");
					
				}
                        }
                        System.out.println();
                }
        }
}
