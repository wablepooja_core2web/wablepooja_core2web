/* row =3
 *
 * 	  A
 * 	B B B
 *    C C C C C
 *
 * row = 4
 *
 *        A
 *      B B B
 *    C C C C C
 *  D D D D D D D 	*/

import java.io.*;
class program4 {
        public static void main(String args[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no of rows :");
                int row = Integer.parseInt(br.readLine());
		
		char ch='A';
                for(int i=1;i<=row;i++) {
                        for(int space=1;space<=row-i;space++) {
                                System.out.print("  ");
                        }
                        
                        for(int j=1;j<=i*2-1;j++) {
                                System.out.print(ch+" ");
                        }
			ch++;
                        System.out.println();
                }
        }
}
