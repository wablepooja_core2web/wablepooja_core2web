class NumberOperations {
    public static void main(String[] args) {
        int num1 = 5; // Predefined positive number
        int num2 = 7; // Predefined positive number

        int result = num1 * num2;
        System.out.println("Result of multiplication: " + result);

        if (result % 2 == 0) {
            System.out.println("The result is even.");
        } else {
            System.out.println("The result is odd.");
        }
    }
}

