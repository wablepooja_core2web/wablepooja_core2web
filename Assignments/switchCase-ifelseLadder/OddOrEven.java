 class OddEvenChecker {
    public static void main(String[] args) {
        int number=45;

        // Using if-else to check if the number is odd or even
        switch (number % 2) {
            case 0:
                System.out.println(number + " is an even number.");
                break;
            default:
                System.out.println(number + " is an odd number.");
                break;
        }
    }
}

