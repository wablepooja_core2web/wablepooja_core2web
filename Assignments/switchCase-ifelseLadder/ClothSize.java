 class ClothSizeConverter {
    public static void main(String[] args) {
        String sizeAcronym = "S"; // Example cloth size acronym
        
        String longForm;

        // Using switch case to convert acronym to long form
        switch (sizeAcronym) {
            case "S":
                longForm = "Small";
                break;
            case "M":
                longForm = "Medium";
                break;
            case "L":
                longForm = "Large";
                break;
            case "XL":
                longForm = "Extra Large";
                break;
            case "XXL":
                longForm = "Extra Extra Large";
                break;
            default:
                longForm = "Unknown size";
                break;
        }

        System.out.println("The long form of " + sizeAcronym + " is: " + longForm);
    }
}

