 class TripPlanner {
    public static void main(String[] args) {
        int budget = 10000; // Example budget
        
        String destination;

        // Using if-else statements to determine the destination based on the budget
        if (budget >= 15000) {
            destination = "Jammu and Kashmir";
        } else if (budget >= 10000) {
            destination = "Manali";
        } else if (budget >= 6000) {
            destination = "Amritsar";
        } else if (budget >= 2000) {
            destination = "Mahabaleshwar";
        } else {
            destination = "Try next time";
        }

        System.out.println("For a budget of " + budget + ", the destination is: " + destination);
    }
}

