class DayOfWeek {
    public static void main(String[] args) {
        int dayNumber = 5; // Example input day number
        
        String dayOfWeek;

        // Using switch case to determine the day of the week
        switch (dayNumber) {
            case 1:
                dayOfWeek = "Sunday";
                break;
            case 2:
                dayOfWeek = "Monday";
                break;
            case 3:
                dayOfWeek = "Tuesday";
                break;
            case 4:
                dayOfWeek = "Wednesday";
                break;
            case 5:
                dayOfWeek = "Thursday";
                break;
            case 6:
                dayOfWeek = "Friday";
                break;
            case 7:
                dayOfWeek = "Saturday";
                break;
            default:
                dayOfWeek = "Invalid day number";
                break;
        }

        System.out.println("The day corresponding to " + dayNumber + " is: " + dayOfWeek);
    }
}

