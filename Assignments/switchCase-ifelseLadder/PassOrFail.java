class ExamGrades {
    public static void main(String[] args) {
        int marksMath = 80;
        int marksEnglish = 75;
        int marksScience = 85;
        int marksHistory = 90;
        int marksGeography = 80;

        int totalMarks = marksMath + marksEnglish + marksScience + marksHistory + marksGeography;

        if (marksMath < 40 || marksEnglish < 40 || marksScience < 40 || marksHistory < 40 || marksGeography < 40) {
            System.out.println("You failed the exam.");
        } else {
            System.out.println("Total marks: " + totalMarks);

            int averageMarks = totalMarks / 5;
            switch (averageMarks / 10) {
                case 10: 
			System.out.println("Topper");
                case 9:
                    System.out.println("Grade: First class with distinction");
                    break;
                case 8:
                case 7:
                    System.out.println("Grade: First class");
                    break;
                case 6:
                    System.out.println("Grade: Second class");
                    break;
                default:
                    System.out.println("Grade: Pass");
                    break;
            }
        }
    }
}

