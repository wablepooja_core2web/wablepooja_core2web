 class NumberOperation {
    public static void main(String[] args) {
        int num1 = 6; // Example first number
        int num2 = 4; // Example second number

        int result;

        // Check if both numbers are positive
        if (num1 >= 0 && num2 >= 0) {
            result = num1 * num2;

            // Using switch-case to check if the result is even or odd
            switch (result % 2) {
                case 0:
                    System.out.println("The result (" + result + ") is even.");
                    break;
                default:
                    System.out.println("The result (" + result + ") is odd.");
                    break;
            }
        } else {
            System.out.println("Sorry, negative numbers not allowed. Program terminated.");
        }
    }
}

