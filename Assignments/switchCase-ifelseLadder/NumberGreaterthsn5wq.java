 class NumberSpelling {
    public static void main(String[] args) {
        
        System.out.print("Enter a number (0 to 5): ");
        int number =13;

        String numberSpelling;
	if(number<=5&& number>=0){
        // Using switch case to determine the spelling of the number
        switch (number) {
            case 0:
                numberSpelling = "Zero";
                break;
            case 1:
                numberSpelling = "One";
                break;
            case 2:
                numberSpelling = "Two";
                break;
            case 3:
                numberSpelling = "Three";
                break;
            case 4:
                numberSpelling = "Four";
                break;
            case 5:
                numberSpelling = "Five";
                break;
            default:
                numberSpelling = "Entered number is greater than 5";
                break;
        }


        System.out.println("The spelling of the number is: " + numberSpelling);
    }else{
	    System.out.println(number +"Number is greater than 5");
}
    }
 }

