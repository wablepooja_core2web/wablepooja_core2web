class StudentGrades {
    public static void main(String[] args) {
        // Predefined marks for 5 subjects
        int subject1 = 78;
        int subject2 = 85;
        int subject3 = 92;
        int subject4 = 63;
        int subject5 = 71;
        
        int totalMarks = subject1 + subject2 + subject3 + subject4 + subject5;

        // Checking if all subjects have passing marks
        boolean allPass = true;
        if (subject1 < 40 || subject2 < 40 || subject3 < 40 || subject4 < 40 || subject5 < 40) {
            allPass = false;
        }

        // If all subjects have passing marks
        if (allPass) {
            // Calculate percentage
            double percentage = (totalMarks / 5.0);
            
            // Using switch-case to print grades based on percentage
            switch ((int) percentage / 10) {
                case 9:
                    System.out.println("Your grade: First Class with Distinction");
                    break;
                case 8:
                case 7:
                    System.out.println("Your grade: First Class");
                    break;
                case 6:
                    System.out.println("Your grade: Second Class");
                    break;
                default:
                    System.out.println("Your grade: Pass");
                    break;
            }
        } else {
            System.out.println("You failed the exam");
        }
    }
}

