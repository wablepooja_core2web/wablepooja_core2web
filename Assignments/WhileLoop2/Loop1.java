class DivisibleBy2{
    public static void main(String[] args) {
        int number = 123456789; // Example number

        while (number > 0) {
            int digit = number % 10;
            if (digit % 2 == 0) {
                System.out.println(digit);
            }
            number /= 10;
        }
    }
}
