class ForWhileDemo{
	  public static void main(String[] args) {
             System.out.println("Using for loop to plan weekday activities:");
        for (int day = 1; day <= 5; day++) { // Monday to Friday
            System.out.print("On ");
            switch (day) {
                case 1:
                    System.out.println("Monday:");
                    System.out.println("- Go to school");
                    break;
                case 2:
                    System.out.println("Tuesday:");
                    System.out.println("- Dance class");
                    break;
                case 3:
                    System.out.println("Wednesday:");
                    System.out.println("- Go to school");
                    break;
                case 4:
                    System.out.println("Thursday:");
                    System.out.println("- Dance class");
                    break;
                case 5:
                    System.out.println("Friday:");
                    System.out.println("- Go to school");
                    break;
            }
        }

        // Using while loop to plan weekend activities
        System.out.println("\nUsing while loop to plan weekend activities:");
        int weekendDays = 2; // Saturday and Sunday
        int day = 6; // Start from Saturday
        while (day <= 7) {
            System.out.print("On ");
            switch (day) {
                case 6:
                    System.out.println("Saturday:");
                    System.out.println("- Soccer practice");
                    break;
                case 7:
                    System.out.println("Sunday:");
                    System.out.println("- Family picnic");
                    break;
            }
            day++;
        }
    }
}

