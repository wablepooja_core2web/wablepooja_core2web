class Population{
    public static void main(String[] args) {
        // Latest population estimates (as of January 2022)
        long worldPopulation = 7_900_000_000L; // 7.9 billion
        long indiaPopulation = 1_366_000_000L; // 1.366 billion

        // Printing population statistics
        System.out.println("World Population: " + worldPopulation);
        System.out.println("India Population: " + indiaPopulation);
    }
}

