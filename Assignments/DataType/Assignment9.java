class ACTemperature {
    public static void main(String[] args) {
        double acTemperature = 22.5;
        double standardTemperature = 25.0;

        System.out.println("AC Temperature: " + acTemperature + " degrees Celsius");
        System.out.println("Standard Temperature: " + standardTemperature + " degrees Celsius");
    }
}

