class GravityValue {
    public static void main(String[] args) {
        // Value of gravity on Earth (approximate)
        double gravity = 9.8;

        // Letter used to represent acceleration due to gravity
        char gravityLetter = 'g';

        // Printing the result
        System.out.println("Value of Gravity: " + gravity + " m/s^2");
        System.out.println("Letter for Acceleration Due to Gravity: " + gravityLetter);
    }
}

