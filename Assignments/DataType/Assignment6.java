 class CalculateSecondsSinceBirth {
    public static void main(String[] args) {
        // Storing birth date, month, and year in variables
        int birthDay = 1, birthMonth = 1, birthYear = 2000;
        int SECONDS_IN_DAY = 24 * 60 * 60;
        int SECONDS_IN_MONTH = SECONDS_IN_DAY * 30; 
        int SECONDS_IN_YEAR = SECONDS_IN_DAY * 365;
        int currentDay = 1, currentMonth = 2, currentYear = 2024;
        int totalSecondsSinceBirth = (currentDay - birthDay) * SECONDS_IN_DAY
                + (currentMonth - birthMonth) * SECONDS_IN_MONTH
                + (currentYear - birthYear) * SECONDS_IN_YEAR;

        System.out.println("Total seconds since birth: " + totalSecondsSinceBirth);
    }
}

