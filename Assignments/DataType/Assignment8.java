class StudentGradeCalculator {
    public static void main(String[] args) {
        double marks = 85.5;
        double totalMarks = 100.0;
        double percentage = (marks / totalMarks) * 100;
        char grade = 'B';
        System.out.println("Percentage: " + percentage + "%");
        System.out.println("Grade: " + grade);
    }
}


