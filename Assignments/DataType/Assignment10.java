class MovieDetails {
    public static void main(String[] args) {
        String movieTitle = "The Movie";
        double boxOfficeCollection = 100000000.00;
        double imdbRating = 8.5;

        System.out.println("Movie Title: " + movieTitle);
        System.out.println("Box Office Collection: $" + boxOfficeCollection);
        System.out.println("IMDb Rating: " + imdbRating);
    }
}

