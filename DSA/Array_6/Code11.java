class Code11 {
    public static void main(String[] args) {
        int arr[] = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int N = arr.length;
        
        System.out.println("Pair sums:");

        for (int i = 0; i < N / 2; i++) {
            int sum = arr[i] + arr[N - 1 - i];
            System.out.println("arr[" + i + "] + arr[" + (N - 1 - i) + "] = " + sum);
        }

        // If the array length is odd, print the middle element
        if (N % 2 != 0) {
            System.out.println("Middle element: arr[" + N / 2 + "] = " + arr[N / 2]);
        }
    }
}

