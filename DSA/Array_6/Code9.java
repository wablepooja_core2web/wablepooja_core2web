class Code9 {
    public static void main(String []args) {
        int arr[] = new int[]{1, 2, 3, 4, 5, 6, 7, 8};
        int N = arr.length;
        
        System.out.println("Original Array:");
        for(int i = 0; i < N; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        
        int i = 0;
        int j = N - 1;
        while(i < j) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            i++;
            j--;
        }
        
        System.out.println("Reversed Array:");
        for(int k = 0; k < N; k++) {
            System.out.print(arr[k] + " ");
        }
        System.out.println();
    }
}

