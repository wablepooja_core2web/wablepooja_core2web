import java.util.*;

class Book {
    private String title;
    private String author;
    private boolean available;

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
        this.available = true;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}

class Library {
    private List<Book> books;

    public Library() {
        this.books = new ArrayList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void displayBooks() {
        System.out.println("Books available in the library:");
        for (Book book : books) {
            System.out.println(book.getTitle() + " by " + book.getAuthor());
        }
    }

    public Book findBook(String title) {
        for (Book book : books) {
            if (book.getTitle().equalsIgnoreCase(title) && book.isAvailable()) {
                return book;
            }
        }
        return null;
    }
}

public class program5 {
    public static void main(String[] args) {
        Library library = new Library();

        // Adding books to the library
        library.addBook(new Book("Java Programming", "John Doe"));
        library.addBook(new Book("Python Programming", "Jane Smith"));
        library.addBook(new Book("Data Structures and Algorithms", "Alice Johnson"));

        // Displaying available books
        library.displayBooks();

        // Example of finding and borrowing a book
        String titleToFind = "Java Programming";
        Book foundBook = library.findBook(titleToFind);
        if (foundBook != null) {
            System.out.println("Book found: " + foundBook.getTitle() + " by " + foundBook.getAuthor());
            foundBook.setAvailable(false); // Marking the book as unavailable (borrowed)
        } else {
            System.out.println("Book with title '" + titleToFind + "' not found or not available.");
        }

        // Displaying available books after borrowing
        library.displayBooks();
    }
}
