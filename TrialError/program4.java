import java.util.*;

class Account {
    private String accountNumber;
    private String holderName;
    private double balance;

    public Account(String accountNumber, String holderName, double balance) {
        this.accountNumber = accountNumber;
        this.holderName = holderName;
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance += amount;
        System.out.println(amount + " deposited successfully.");
    }

    public void withdraw(double amount) {
        if (balance >= amount) {
            balance -= amount;
            System.out.println(amount + " withdrawn successfully.");
        } else {
            System.out.println("Insufficient funds.");
        }
    }

    public void displayAccountInfo() {
        System.out.println("Account Number: " + accountNumber);
        System.out.println("Holder Name: " + holderName);
        System.out.println("Balance: $" + balance);
    }
}

class Bank {
    private Map<String, Account> accounts;

    public Bank() {
        this.accounts = new HashMap<>();
    }

    public void addAccount(Account account) {
        accounts.put(account.getAccountNumber(), account);
    }

    public Account getAccount(String accountNumber) {
        return accounts.get(accountNumber);
    }
}

public class program4 {
    public static void main(String[] args) {
        Bank bank = new Bank();

        // Creating and adding accounts
        Account account1 = new Account("1001", "John Doe", 1000);
        Account account2 = new Account("1002", "Jane Smith", 2000);
        bank.addAccount(account1);
        bank.addAccount(account2);

        // Displaying account information
        System.out.println("Account Information:");
        bank.getAccount("1001").displayAccountInfo();
        bank.getAccount("1002").displayAccountInfo();

        // Depositing and withdrawing from account
        System.out.println("\nPerforming transactions:");
        bank.getAccount("1001").deposit(500);
        bank.getAccount("1002").withdraw(1000);

        // Displaying updated account information
        System.out.println("\nUpdated Account Information:");
        bank.getAccount("1001").displayAccountInfo();
        bank.getAccount("1002").displayAccountInfo();
    }
}
