class program3 {
    public static void main(String[] args) {
        System.out.println("Even numbers between 1 and 10:");
        
        for (int i = 1; i <= 10; i++) {
            if (i % 2 == 0) {
                continue; // Skip the rest of the loop body and continue with the next iteration
            }
            System.out.println(i);
        }
    }
}

